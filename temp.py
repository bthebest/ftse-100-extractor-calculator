a = '''Name	Ticker
Admiral Group	ADM.L
Anglo American	AAL.L
Antofagasta Holdings	ANTO.L
Ashtead Group plc	AHT.L
Associated British Foods plc	ABF.L
AstraZeneca plc	AZN.L
Auto Trader Group plc	AUTO.L
AVEVA Group plc	AVV.L
Aviva plc	AV..L
BAE Systems plc	BA..L
Barclays plc	BARC.L
Barratt Developments plc	BDEV.L
Berkeley Group Holdings plc	BKG.L
BHP Group Plc	BHP.L
BP Plc	BP..L
British American Tobacco plc	BATS.L
British Land Co plc	BLND.L
BT Group plc	BT.A.L
Bunzl plc	BNZL.L
Burberry Group plc	BRBY.L
Carnival plc	CCL.L
Centrica plc	CNA.L
Coca-Cola HBC AG	CCH.L
Compass Group plc	CPG.L
CRH plc	CRH.L
Croda International plc	CRDA.L
DCC plc	DCC.L
Diageo plc	DGE.L
Evraz plc	EVR.L
Experian Plc	EXPN.L
Ferguson plc	FERG.L
Flutter Entertainment	FLTR.L
Fresnillo	FRES.L
GlaxoSmithKline plc	GSK.L
Glencore plc	GLEN.L
Halma plc	HLMA.L
Hargreaves Lansdown plc	HL..L
Hikma Pharmaceuticals	HIK.L
Hiscox Ltd	HSX.L
HSBC Holdings plc	HSBA.L
Imperial Brands Group	IMB.L
Informa plc	INF.L
InterContinental Hotels Group plc	IHG.L
International Consolidated Airlines Group SA	IAG.L
Intertek Group plc	ITRK.L
ITV plc	ITV.L
JD Sports Fashion plc	JD..L
Johnson Matthey Plc	JMAT.L
Kingfisher	KGF.L
Land Securities Group plc	LAND.L
Legal & General Group plc	LGEN.L
Lloyds Banking Group plc	LLOY.L
London Stock Exchange Group plc	LSE.L
M&G plc	MNG.L
Meggitt	MGGT.L
Melrose Industries plc	MRO.L
Mondi Plc	MNDI.L
Morrison (Wm) Supermarkets	MRW.L
National Grid	NG..L
Next plc	NXT.L
NMC Health Plc	NMC.L
Ocado Group plc	OCDO.L
Pearson plc	PSON.L
Persimmon plc	PSN.L
Phoenix Group Holdings Plc	PHNX.L
Polymetal International plc	POLY.L
Prudential plc	PRU.L
Reckitt Benckiser Group Plc	RB..L
RELX plc	REL.L
Rentokil Initial Plc	RTO.L
Rightmove plc	RMV.L
Rio Tinto plc	RIO.L
Rolls Royce Holdings Plc	RR..L
Royal Bank of Scotland Group plc	RBS.L
Royal Dutch Shell Plc A Shares	RDSa.L
Royal Dutch Shell Plc B Shares	RDSb.L
RSA Insurance Group	RSA.L
Sage Group plc	SGE.L
Sainsbury (J) plc	SBRY.L
Schroders plc	SDR.L
Scottish Mortgage Investment Trust	SMT.L
Segro Plc	SGRO.L
Severn Trent Plc	SVT.L
Smith & Nephew plc	SN..L
Smith (DS)	SMDS.L
Smiths Group Plc	SMIN.L
Smurfit Kappa Group Plc	SKG.L
Spirax-Sarco Engineering plc	SPX.L
SSE plc	SSE.L
St James's Place Plc	STJ.L
Standard Chartered plc	STAN.L
Standard Life Aberdeen Plc	SLA.L
Taylor Wimpey plc	TW..L
Tesco plc	TSCO.L
TUI AG	TUI.L
Unilever plc	ULVR.L
United Utilities Group Plc	UU..L
Vodafone Group plc	VOD.L
Whitbread plc	WTB.L
WPP plc	WPP.L'''
import csv

wr = csv.writer(open('ticker.csv', 'w'))
b = a.split('\n')
for i in b:
    x = i.split('\t')
    x[1] = x[1].replace('..', '.')
    wr.writerow(x)