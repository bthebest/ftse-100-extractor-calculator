import requests
import csv
import codecs
import json
from time import sleep
from datetime import datetime
import pandas as pd
import numpy as np
from statistics import variance
from multiprocessing import Process, Manager
#import glob
from statistics import stdev
from xlsxwriter import Workbook

'''FILTER FUNCTION FROM LIBRARIES DONT IMPORT WHOLE'''

dt = datetime

#CHECK TICKER
tickerFile = open('ticker.csv')
tickers = list(csv.reader(tickerFile))
tickers = list(map(list, zip(*tickers)))
companyNames = tickers[0][1:]
tickerNames = tickers[1][1:]
company_output_dict = dict.fromkeys(companyNames)

#IMPORT TIMESLOTS AND INTERVAL
timeJson = open('timeslots.json')
timeObj = json.load(timeJson)
#print(timeObj)
startDate = int(dt.timestamp(dt.strptime(timeObj['interval']['start'], '%d-%m-%Y')))
endDate = int(dt.timestamp(dt.strptime(timeObj['interval']['end'], '%d-%m-%Y'))) + 64800
numberOfDays = int((endDate - startDate) / 86400)
print(numberOfDays)
#slots
slots = timeObj['slots']
slot1 = slots["slot1(days)"]
slot2 = slots["slot2(months)"] * 30
slot3 = slots["slot3(months)"] * 30
slot4 = slots["slot4(months)"] * 30
slot5 = slots["slot5(years)"] * 365
#slot6 = slots["slot6(years)"] * 365

slots = [slot1, slot2, slot3, slot4, slot5]

if max(slot1, slot2, slot3, slot4, slot5) > numberOfDays:
    print("SLOT VALUES ARE INCORRECT...!")
    sleep(3)
    exit()

#FILE SAVER
def saveFile(name, path, data, start, end, header=None):
    fileName = name + '__' + \
               str(dt.utcfromtimestamp(start).strftime('%Y-%m-%d')) + '_to_' + \
               str(dt.utcfromtimestamp(end).strftime('%Y-%m-%d'))
    fileObj = open(path +
                   fileName +
                   '.csv', 'w', newline='')
    if header:
        csv.writer(fileObj).writerows([[header],[]])

    csv.writer(fileObj).writerows(data)

def cleanAll():
    import shutil
    from os.path import exists
    from os import makedirs

    dir_path = 'data'
    if exists('data'):
        try:
            shutil.rmtree(dir_path)
        except OSError as e:
            print("Error: %s : %s" % (dir_path, e.strerror))

    makedirs("data/raw_data", exist_ok=True)
    makedirs("data/calculated_data", exist_ok=True)
    makedirs("data/calculated_data/summary_files", exist_ok=True)

def summaryFileMake(data1, data2, summary, company1, company2):
    headers = [[company1, '', '', '', company2, '', ''],
               ['Date', 'Closing', '% Change', '', 'Date', 'Closing', '% Change']]
    data = np.concatenate((data1, [['']]*len(data1), data2), axis=1)
    data = np.concatenate((headers, data), axis=0).tolist()
    data = data + [[]] + [['Summary']] + summary.tolist()
    return data

#SCRAPE FOR EVERY COMPANY
def scrapeCompany(name, ticker, start, end):

    page = requests.get('https://query1.finance.yahoo.com/v7/finance/download/' + ticker + '?period1=' + str(start) +'&period2=' + str(end) + '&interval=1d&events=history')
    #print(page)
    reader = list(csv.reader(codecs.iterdecode(page.iter_lines(), 'utf-8')))
    saveFile(name, 'data/raw_data/', reader, start, end)

    return reader
    #print(reader)
    #print(len(reader))

#DO CALCULATIONS
def calculate(data):
    data_df = pd.DataFrame(data)
    data_df = data_df.drop([1, 2, 3, 5, 6], axis=1).drop([0])
    change_percent = round(data_df[4].rolling(1, axis=0).sum().pct_change(axis=0) * 100, 2)
    data_df[5] = change_percent
    data = data_df.values.tolist()
    return data

#SAVE CALCULATED FILES / MAKE SUMMARY
def make_summary(cmpData, ftseData, slots):
    npArrayCompany = np.array(cmpData).T
    npArrayFtse = np.array(ftseData).T

    cmp = npArrayCompany[-1]
    ftse = npArrayFtse[-1]

    cmpTodayPrice = npArrayCompany[1][-1]
    ftseTodayPrice = npArrayFtse[1][-1]

    #results = np.array([])
    results = []
    for sl in slots:
        if sl >= len(cmp):
            sl = len(cmp)-1

        cmp_n_price = npArrayCompany[1][-(sl + 1)]
        ftse_n_price = npArrayFtse[1][-(sl + 1)]
        slotArray = slotCalculator(cmp, ftse, cmpTodayPrice, ftseTodayPrice, cmp_n_price, ftse_n_price, sl)
        results.append(slotArray)
        #np.append(results, np.array(slotArray.copy()), axis=0)

    res = [[row[i] for row in results] for i in range(len(results[0]))]
    final_gross = 0

    for row in res[:-1]:
        count = sum(x>0 for x in row)
        summation = round(sum(row) * 100, 3)
        total = round(count + summation, 3)
        row.append(count)
        row.append(summation)
        row.append(total)

        if res.index(row) in [0, 3, 6, 7, 10, 13, 16, 17, 18, 19]:
            final_gross += total

    res[-1].append(0)
    res[-1].append(0)
    res[-1].append(0)

    final_gross = round(final_gross, 2)
    #print('results',  res)

    return res, final_gross

def cov(x, y):
    x_mean = sum(x)/len(x)
    y_mean = sum(y)/len(y)
    data = [(x[i] - x_mean) * (y[i] - y_mean)
            for i in range(len(x))]
    return sum(data) / (len(data) - 1)

def slotCalculator(cmp, ftse, cmp_today, ftse_today, cmp_n, ftse_n, slot):
    risk_free = 0.72 / 100
    cmp_today = float(cmp_today)
    ftse_today = float(ftse_today)
    cmp_n = float(cmp_n)
    ftse_n = float(ftse_n)

    cmp = [float(i) for i in cmp]
    ftse = [float(i) for i in ftse]

    excess_return = np.subtract(cmp, ftse)
    cmp_n_to_today_change = ((cmp_today - cmp_n) / cmp_n)
    ftse_n_to_today_change = ((ftse_today - ftse_n) / ftse_n)

    cmp_positive_changes = [x for x in cmp[-slot:] if x>0]
    cmp_negative_changes = [x for x in cmp[-slot:] if x<0]
    ftse_positive_changes = [x for x in ftse[-slot:] if x>0]
    ftse_negative_changes = [x for x in ftse[-slot:] if x<0]

    v1 = cmp_performance_vs_ftse = cmp_n_to_today_change - ftse_n_to_today_change
    v2 = cmp_sharp_ratio = ((cmp_n_to_today_change) - (risk_free * slot / 365)) / stdev(cmp[-slot:])
    v3 = ftse_sharp_ratio = ((ftse_n_to_today_change) - (risk_free * slot / 365)) / stdev(ftse[-slot:])
    v4 = cmp_sharp_alpha = cmp_sharp_ratio - ftse_sharp_ratio

    v5 = cmp_sortino_ratio = ((cmp_n_to_today_change) - (risk_free * slot / 365)) / (stdev(cmp_negative_changes) if (len(cmp_negative_changes) > 1 and stdev(cmp_negative_changes) > 0) else 0.0001)
    v6 = ftse_sortino_ratio = ((ftse_n_to_today_change) - (risk_free * slot / 365)) / (stdev(ftse_negative_changes) if (len(ftse_negative_changes) > 1 and stdev(ftse_negative_changes) > 0) else 0.0001)
    v7 = cmp_sortino_alpha = cmp_sortino_ratio - ftse_sortino_ratio

    v8 = cmp_info_ratio = cmp_performance_vs_ftse / stdev(excess_return[-slot:])

    v9 = cmp_calmer_ratio = sum(cmp[-slot:]) / slot * 10000 if min(cmp[-slot:]) >= 0 else sum(cmp[-slot:]) / slot / abs(min(cmp[-slot:]))
    v10 = ftse_calmer_ratio = sum(ftse[-slot:]) / slot * 10000 if min(ftse[-slot:]) >= 0 else sum(ftse[-slot:]) / slot / abs(min(ftse[-slot:]))
    v11 = cmp_calmer_alpha = cmp_calmer_ratio - ftse_calmer_ratio

    if len(cmp_positive_changes) <= 0:
        v12 = cmp_gtol_ratio = 0
    else:
        v12 = cmp_gtol_ratio = sum(cmp_positive_changes) / len(cmp_positive_changes) if min(cmp[-slot:]) >= 0 else sum(cmp_positive_changes) / len(cmp_positive_changes) / abs(sum(cmp_negative_changes) / len(cmp_negative_changes))

    if len(ftse_positive_changes) <= 0:
        v13 = ftse_gtol_ratio = 0
    else:
        v13 = ftse_gtol_ratio = sum(ftse_positive_changes) / len(ftse_positive_changes) if min(ftse[-slot:]) >= 0 else sum(ftse_positive_changes) / len(ftse_positive_changes) / abs(sum(ftse_negative_changes) / len(ftse_negative_changes))
    v14 = cmp_gtol_alpha = cmp_gtol_ratio - ftse_gtol_ratio

    v15 = cmp_max_drawdown = min(cmp[-slot:]) / 100
    v16 = ftse_max_drawdown = min(ftse[-slot:]) / 100
    v17 = cmp_max_drawdown_alpha = cmp_max_drawdown - ftse_max_drawdown

    v18 = cmp_modigliana_rap = cmp_sharp_ratio * stdev(ftse[-slot:]) + (risk_free * slot / 365)
    numberofpositives = sum(x>0 for x in cmp[-slot:])
    numberofnegatives =  sum(x<0 for x in cmp[-slot:])
    v19 = cmp_up_down_ratio = numberofpositives / numberofnegatives if numberofnegatives > 0 else 1

    v20 = covariance = cov(cmp[-slot:], ftse[-slot:]) / variance(ftse[-slot:])
    v21 = cmp_treynoe_ratio = (sum(cmp[-slot:])/slot - sum(ftse[-slot:])/slot) / covariance

    res_arr = [v1, v2, v3, v4, v5, v6, v7, v8, v9, v10,
               v11, v12, v13, v14, v15, v16, v17, v18, v19, v21,
               v20]
    res_arr = [round(i, 3) for i in res_arr]

    return res_arr

#MERGE ALL CSV FILES -- AFTER ALL EXECUTED (MULTIPROCESSING)

def main_processor(startTime, endTime, slots, companyName, tickerName, ftse_data, que):
    print(companyName)
    rawData = scrapeCompany(companyName, tickerName, startTime, endTime)
    calculated_data = calculate(rawData)

    if len(calculated_data) > len(ftse_data):
        print("Something wrong with time slots and interval for ", companyName)
        sleep(3)
        return

    ftse_data = ftse_data.copy()[-len(calculated_data):]
    summary, final_gross = make_summary(calculated_data, ftse_data, slots)
    #company_output_dict[companyName] = final_gross.copy()
    que[companyName] = final_gross

    col_header = slots + ['count', 'sum', 'total']
    summary.insert(0, col_header)
    t = tickerName.replace('.L', '')

    var_names = [[''],
                 [t + ' Performance vs FTSE'],
                 [t + ' Sharpe Ratio'],
                 ['FTSE Sharpe Ratio'],
                 [t + ' Sharpe Alpha'],
                 [t + ' Sortino Ratio'],
                 ['FTSE Sortino Ratio'],
                 [t + ' Sortino Alpha'],
                 [t + ' Information Ratio'],
                 [t + ' Calmar Ratio'],
                 ['FTSE Calmar Ratio'],
                 [t + ' Calmar Alpha'],
                 [t + ' Gain-to-loss Ratio'],
                 ['FTSE Gain-to-loss Ratio'],
                 [t + ' Gain-to-loss Alpha'],
                 [t + ' Max Drawdown'],
                 ['FTSE Max Drawdown'],
                 [t + ' Max Drawdown Alpha'],
                 [t + ' Modigliana RAP'],
                 ['Up-down ratio'],
                 [t + ' Treynor Ratio'],
                 ['Beta']]

    summary_data = np.concatenate((var_names, summary), axis=1)
    summary_data = np.concatenate((summary_data, [['']*len(slots) + ['']*3 + [final_gross]]))
    saveFile(companyName, 'data/calculated_data/summary_files/', summary_data, startTime, endTime, header=companyName)

    readyFileData = summaryFileMake(calculated_data, ftse_data, summary_data, companyName, 'FTSE')
    saveFile(companyName, 'data/calculated_data/', readyFileData, startTime, endTime)

    #print(len(calculated_data), len(ftse_data), companyName, tickerName)

    #print(temp)

def main(companyNames, tickerNames, startTime, endTime, slots, ftse_data):
    manager = Manager()
    return_dict = manager.dict()
    return_dict['Company'] = 'Value'

    processArr = []

    #for i in temp:
    #    main_processor(startTime, endTime, slots, companyNames[i-1], tickerNames[i-1], ftse_data)
    #main_processor(startTime, endTime, slots, companyNames[67], tickerNames[67], ftse_data, return_dict)
    #main_processor(startTime, endTime, slots, companyNames[2], tickerNames[2], ftse_data, return_dict)
    for company, ticker in zip(companyNames, tickerNames):
        p = Process(target=main_processor, args=(startTime, endTime, slots, company, ticker, ftse_data, return_dict,))
        p.start()
        processArr.append(p)

    print("wating to finish...")
    for p in processArr:
        p.join()
    print('All completed')

    with open('Outputs/Final_values.csv', 'w', newline="") as csv_file:
        writer = csv.writer(csv_file)
        for key, value in return_dict.items():
            writer.writerow([key, value])

    from os import listdir

    base_path = "data/calculated_data/"
    file_names = listdir("data/calculated_data/")
    #files = glob.glob("data/calculated_data/*.csv")
    csv_to_excel(base_path, 'Final_calculated_sheet', file_names)

    base_path = "data/calculated_data/summary_files/"
    file_names = listdir("data/calculated_data/summary_files/")
    #files = glob.glob("data/calculated_data/summary_files/*.csv")
    csv_to_excel(base_path, 'Final_summaries', file_names)

def csv_to_excel(base, outputName, names):
    workbook = Workbook('Outputs/' + outputName + '.xlsx')
    for name in names:
        csv_file = base + name
        if csv_file.endswith(".csv"):
            sheet_name = name.split('_')[0][:30]
            worksheet = workbook.add_worksheet(sheet_name)
            with open(csv_file, 'rt', encoding='utf8') as f:
                reader = csv.reader(f)
                for r, row in enumerate(reader):
                    for c, col in enumerate(row):
                        worksheet.write(r, c, col)
    workbook.close()

if __name__ == '__main__':
    cleanAll()
    print('cleaned')

    # FTSE DATA
    ftse_data = scrapeCompany("FTSE100", "^FTSE?P=FTSE", startDate, endDate)
    # print("FTSE DATA", ftse_data)
    ftse_calculated_data = calculate(ftse_data)
    #print(ftse_calculated_data)

    main(companyNames, tickerNames, startDate, endDate, slots, ftse_calculated_data)
    #print(company_output_dict)
