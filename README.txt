To run this project you must have installed python and relevant python libraries listed in "requirements.txt"

To install python 3.7 on windows follow this guide (version 3.7.3 recommended):
https://phoenixnap.com/kb/how-to-install-python-3-windows

To install relevant libraries of python, install PIP (it makes easy to install required libraries):
https://pip.pypa.io/en/stable/installing/

To install libraries after installation of pip:
1. Navigate to project folder through command line and type "pip install requirements.txt"
2. if that doesn't work install every library with name from "requirements.txt". For e.g,
    pip install requests
    pip install codecs
    etc...

to run project:
1. Click on main.py (windows runs directly)
2. Or type "python main.py" in command line

Note: Please make copy of your downloaded/calculated data. It will be cleared when you run for different values again